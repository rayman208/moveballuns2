package com.example.moveballonsapp;

import android.graphics.Canvas;

import java.util.ArrayList;

public class FunnySquaresList
{
    private ArrayList<FunnySquare> funnySquares;

    public FunnySquaresList(int size)
    {
        funnySquares = new ArrayList<>();

        for (int i = 0; i < size; i++)
        {
            funnySquares.add(new FunnySquare());
        }
    }

    public void DrawAll(Canvas canvas)
    {
        for (int i = 0; i < funnySquares.size(); i++)
        {
            funnySquares.get(i).Draw(canvas);
        }
    }

    public void MoveAll(int xMin, int yMin, int xMax, int yMax)
    {
        for (int i = 0; i < funnySquares.size(); i++)
        {
            funnySquares.get(i).Move(xMin,yMin,xMax,yMax);
        }
    }

    public boolean DeleteTouched(int x, int y)
    {
        for (int i = 0; i < funnySquares.size(); i++)
        {
            if(funnySquares.get(i).IsPointInArea(x,y))
            {
                funnySquares.remove(i);
                return true;
            }
        }
        return false;
    }

    public void GenerateNewIfSmallerThanBorder(int border, int size)
    {
        if(funnySquares.size()<border)
        {
            for (int i = 0; i < size; i++)
            {
                funnySquares.add(new FunnySquare());
            }
        }
    }
}
