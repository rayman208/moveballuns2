package com.example.moveballonsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        setContentView(new MyCanvas(getApplicationContext()));
    }

    public class MyCanvas extends View{

        int xMin, yMin, xMax, yMax;
        FunnySquaresList funnySquaresList;
        int countBursted;
        Paint textPaint;

        public MyCanvas(Context context) {
            super(context);

            xMin = 0;
            yMin = 0;

            funnySquaresList = new FunnySquaresList(5);

            countBursted=0;

            textPaint = new Paint();
            textPaint.setARGB(255,127,127,127);
            textPaint.setTextSize(150);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);

            canvas.drawText("Bursted: "+countBursted, xMax/2-320, yMax/2, textPaint);

            funnySquaresList.DrawAll(canvas);

            funnySquaresList.MoveAll(xMin,yMin,xMax,yMax);

            invalidate();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {

            int x = (int)event.getX();
            int y = (int)event.getY();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    if(funnySquaresList.DeleteTouched(x,y))
                    {
                        countBursted++;
                    }
                    funnySquaresList.GenerateNewIfSmallerThanBorder(1,5);

                    invalidate();
                    break;
            }

            return super.onTouchEvent(event);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            xMax = w;
            yMax = h;
        }

    }
}
