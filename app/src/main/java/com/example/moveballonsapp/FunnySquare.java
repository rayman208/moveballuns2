package com.example.moveballonsapp;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

public class FunnySquare
{
    static private Random random = new Random();

    private int GetRandomInRage(int min, int max)
    {
        return random.nextInt((max - min) + 1) + min;
    }

    private int left, width, top, height;
    private int dx, dy;
    private Paint paint;

    public FunnySquare()
    {
        left = 0;
        top = 0;

        width = GetRandomInRage(50,200);
        height = width;
        //height = GetRandomInRage(50,200);

        dx = GetRandomInRage(3,10);
        dy = GetRandomInRage(3,10);

        paint = new Paint();
        paint.setARGB(255, GetRandomInRage(0,255), GetRandomInRage(0,255), GetRandomInRage(0,255));
    }

    public void Move(int xMin, int yMin, int xMax, int yMax)
    {
        left+=dx;
        top+=dy;

        if(left+width>=xMax || left<=xMin){
            dx=-dx;
        }

        if(top+height>=yMax || top<=yMin){
            dy=-dy;
        }
    }

    public void Draw(Canvas canvas)
    {
        canvas.drawOval(left,top,left+width,top+height, paint);
    }

    public boolean IsPointInArea(int x, int y)
    {
        return x>=left && x<=left+width && y>=top && y<=top+height;
    }
}
